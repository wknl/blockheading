# How to use

## block.tpl.php

Alter your block.tpl.php with the following

**Change**  
```php
<h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
```

**To**  
```php
<<?php print $title_heading; ?><?php print $title_attributes; ?>><?php print $block->subject ?></<?php print $title_heading; ?>>
```

Panel blocks should work out of the box.

## Theme preprocess_block

Add or append yourtheme_preprocess_block() with the following

```php
<?php
function yourtheme_preprocess_block(&$variables) {
  if (!module_exists('blockheading')) {
    $variables['title_heading'] = 'h2';
  }
}
```